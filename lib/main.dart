import 'dart:js';

import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Named Routes Demo',
    initialRoute: '/',
    routes: {
      '/': (context) => FirstScreen(),
      '/second': (context) => SecondScreen(),
      '/third': (context) => ThirdScreen()
    },
  ));
}

class FirstScreen extends StatelessWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Screen'),
      ),
      body: Center(
        child: ElevatedButton(
          child: Text('Launch Screen'),
          onPressed: () => {
            Navigator.pushNamed(context, '/second'),
          },
        ),
      ),
    );
  }
}

class SecondScreen extends StatelessWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Second Screen'),
      ),
      body: Center(
        child: Column(
          children: [
            ElevatedButton(
              onPressed: () => {
                Navigator.pushNamed(context, '/third'),
              },
              child: Text('Go to Third Screen'),
            ),
            SizedBox(
              height: 10.0,
            ),
            ElevatedButton(
              child: Text('Go back!'),
              onPressed: () => {
                Navigator.pop(context),
              },
            ),
          ],
        ),
      ),
    );
  }
}

class ThirdScreen extends StatelessWidget {
  const ThirdScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Third Screen'),
      ),
      body: Center(
        child: Column(
          children: [
            ElevatedButton(
              child: Text('Go to Second Screen'),
              onPressed: () => {
                Navigator.pushNamed(context, '/second'),
              },
            ),
            SizedBox(
              height: 10.0,
            ),
            ElevatedButton(
              child: Text('Go Back!'),
              onPressed: () => {
                Navigator.pop(context),
              },
            ),
          ],
        ),
      ),
    );
  }
}
